import 'package:flutter/material.dart';
import 'package:flutter_application_1/home.dart';
import 'package:flutter_application_1/login.dart';
import 'package:flutter_application_1/pictures.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Dashboard"),
          elevation: 0,
          brightness: Brightness.light,
          backgroundColor: const Color.fromARGB(255, 243, 147, 200),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              size: 20,
              color: Colors.black,
            ),
          ),
        ),
        body: Center(
          child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/nice.jpeg"), fit: BoxFit.cover),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: MaterialButton(
                          height: 100.0,
                          minWidth: 100.0,
                          color: const Color.fromARGB(300, 11, 70, 75),
                          textColor: Colors.white,
                          child: const Text("Profile"),
                          onPressed: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: ((context) => const HomePage()),
                              ),
                            ),
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: MaterialButton(
                          height: 100.0,
                          minWidth: 100.0,
                          color: const Color.fromARGB(300, 11, 70, 75),
                          textColor: Colors.white,
                          child: const Text("LogOut"),
                          onPressed: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: ((context) => const LoginPage()),
                              ),
                            ),
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: MaterialButton(
                          height: 100.0,
                          minWidth: 100.0,
                          color: const Color.fromARGB(300, 11, 70, 75),
                          textColor: Colors.white,
                          child: const Text("Pictures"),
                          onPressed: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: ((context) => const Pictures()),
                              ),
                            ),
                          },
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 100),
                    height: 300,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/reads.gif"),
                          fit: BoxFit.fitHeight),
                    ),
                  )
                ],
              )),
        ));
  }
}
