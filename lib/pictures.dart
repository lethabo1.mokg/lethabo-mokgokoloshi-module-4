import 'package:flutter/material.dart';

class Pictures extends StatelessWidget {
  const Pictures({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // padding: const EdgeInsets.only(top: 100),
        // height: 200,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/flutback.png"), fit: BoxFit.cover),
        ),
      ),
      appBar: AppBar(
        title: const Text("Gallery"),
        elevation: 1,
        backgroundColor: const Color.fromARGB(255, 243, 147, 200),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color.fromARGB(255, 87, 54, 54),
          ),
          onPressed: () => {Navigator.of(context).pop()},
        ),
      ),
    );
  }
}
